# python virtual environment
- python.exe -m pip install --upgrade pip
- .\Scripts\activate

# python app

- install Flask 
``` python
pip install flask 

pip freeze > requirements.txt
```


# git init and .gitignore

# docker

docker build -t arjun207/flaskwebapp:1.0 .
docker run -d -p 80:5000 --name myweb arjun207/flaskwebapp:1.0

docker ps -a

docker images

docker exec -it myweb sh

docker stop flaskwebapp
docker rm -f flaskwebapp
# Kubernetes

kubectl apply -f .\deployment.yaml
kubectl get po
kubectl apply -f .\service.yaml
kubectl get po.svc
kubectl get deployments
kubectl delete deploy mywebapp
kubectl delete service my-service

## gitlab ci