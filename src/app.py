from flask import Flask,jsonify,render_template
import socket
app = Flask(__name__)

# function to fetch hostname and ip address
def fetch_details():
    hostname = socket.gethostname()
    hostip = socket.gethostbyname(hostname)
    return str(hostname),str(hostip)

# test route
@app.route("/")
def hello_world():
    return "<h1>Hello, World!</h1>"

# health - json route
@app.route("/health")
def health():
    return jsonify(
        status = "ok"
    )

# details - static html route with jinja templating
@app.route("/details")
def details():
    name,ip = fetch_details()
    return render_template("index.html",hostname=name,hostip=ip)


if __name__ == "__main__":
    app.run(host="0.0.0.0",port=5000)